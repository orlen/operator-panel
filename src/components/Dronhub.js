import React, { Component } from 'react';

class Dronhub extends Component {
    render() {
      const heartbeat = this.props.heartbeat;
      let batteryCharging = '-';
      const isBattery = heartbeat.drone_battery != null;
      if(isBattery){
        batteryCharging = heartbeat.drone_battery.charging ? 'TAK' : 'NIE'
      }
        return (
            <div className='Dronhub Info'>
              <p className='InfoTitle'>Dronhub</p>
              <div className='InfoContent'>
                <div className='LeftColumn'>
                  <p>ID Drona: </p>
                  <p>Status: </p>
                  <p>Ładowanie: </p>
                  <p>Prąd ładowania: </p>
                  <p>Napięcie ładowania: </p>
                </div>
                <div className='RightColumn'>
                  <p><b>{heartbeat.drone_id!=null?heartbeat.drone_id:0}</b></p>
                  <p><b>{heartbeat.state!=null?heartbeat.state:0}</b></p>
                  <p><b>{batteryCharging}</b></p>
                  <p><b>{isBattery && heartbeat.drone_battery.current!=null ? heartbeat.drone_battery.current.toFixed(2) : '-' }</b>A</p>
                  <p><b>{isBattery && heartbeat.drone_battery.voltage!=null ? heartbeat.drone_battery.voltage.toFixed(2) : '-' }</b>V</p>
                </div>
              </div>
            </div>
        );
    }
}

export default Dronhub;
