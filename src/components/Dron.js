import React, {Component} from 'react';

class Dron extends Component {
  dir = (n,e) => {
    const pi = Math.PI;
    return Math.atan2(e,n)/pi*180;
  };
  spd = (n,e) => {
    return Math.sqrt(n*n+e*e);
  };
  render() {
    const system = this.props.system;
    const battery = this.props.battery;
    const flight = this.props.flight;
    return (
      <div className='Dronhub Info'>
        <p className='InfoTitle'>Dron</p>
        <div className='InfoContent'>
          <div className='LeftColumn'>
            <p>Prędkość pozioma: </p>
            <p>Prędkość pionowa: </p>
            <p>AGL : </p>
            <p>Kierunek lotu: </p>
            <p>Zwrot: </p>
            <p>Napięcie baterii: </p>
            <p>Prąd pobierany:  </p>
            <p>Dostępne satelity: </p>
            <p>HDOP : </p>
            <p>Status: </p>
          </div>
          <div className='RightColumn'>
            <p><b>{flight.vel_ned!=null?this.spd(flight.vel_ned[0],flight.vel_ned[1]).toFixed(2):'-'}</b> m/s</p>
            <p><b>{flight.vel_ned!=null?flight.vel_ned[2].toFixed(2):'-'}</b> m/s</p>
            <p><b>{flight.alt_local!=null?flight.alt_local.toFixed(2):'-'}</b> m</p>
            <p><b>{flight.vel_ned!=null?this.dir(flight.vel_ned[0],flight.vel_ned[1]).toFixed(2)+'°':'-'}</b></p>
            <p><b>{flight.yaw!=null?(flight.yaw/Math.PI*180).toFixed(2)+'°':'-'}</b></p>
            <p><b>{battery.battery_voltage!=null?battery.battery_voltage.toFixed(2):'-'}</b> V</p>
            <p><b>{battery.battery_current!=null?battery.battery_current.toFixed(2):'-'}</b> A</p>
            <p><b>{system.satellites_visible!=null?system.satellites_visible:'-'}</b></p>
            <p><b>{flight.hdop!=null?flight.hdop:'-'}</b></p>
            <p><b>{system.state!=null?system.state:'-'}</b></p>
          </div>
        </div>
      </div>
    );
  }
}

export default Dron;
