import React, {Component} from 'react';

class EditMission extends Component {
  constructor(props) {
    super(props);
    const hours = Math.floor(this.props.defTime / 3600);
    const minutes = Math.floor((this.props.defTime - hours * 3600) / 60);
    const hrs = hours < 10 ? "0" + hours : hours;
    const mins = minutes < 10 ? "0" + minutes : minutes;
    this.state = {
      newTime: hrs + ':' + mins,
      newSeconds: this.props.defTime,
    }
  }

  handleTimeChange = e => {
    this.setState({newSeconds: e.target.valueAsNumber / 1000, newTime: e.target.value});
  };

  render() {
    const x = this.props.editedField.x;
    const y = this.props.editedField.y;
    const left = this.props.editedField.left;
    const right = this.props.editedField.right;
    const top = this.props.editedField.top;
    const bottom = this.props.editedField.bottom;
    const width = this.props.editedField.width;
    const height = this.props.editedField.height;

    return (
      <div className='EditMission'>
        <div className='EditMissionInner' style={{
          x: x + 'px',
          y: y + 'px',
          left: left - width / 2 + 'px',
          right: right + 'px',
          top: top + 'px',
          bottom: bottom + 'px',
          width: 2 * width + 'px',
          height: height + 'px',
        }}>
          <input type='time' value={this.state.newTime} onChange={this.handleTimeChange}/>
        </div>
        <div className='ButtonsEditMission' style={{
          x: x + 'px',
          y: y + 'px',
          left: (left - width) + 'px',
          right: right + 'px',
          top: top + (2 * height) + 'px',
          width: (3 * width) + 'px',
        }}>
          <button className='ButtonEditMission'
                  onClick={() => this.props.confirmEditMission(this.props.id, this.state.newSeconds)}>Zatwierdź
          </button>
          <button className='ButtonEditMission' onClick={this.props.closeEditMission}>Wróć</button>
        </div>
      </div>
    );
  }
}

export default EditMission;
