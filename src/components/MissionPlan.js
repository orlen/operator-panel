import React, {Component} from 'react';
import EditMission from './EditMission';
import axios from 'axios';

import moment from 'moment';
import {Redirect} from "react-router-dom";

class MissionPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdited: false,
      plannedMissions: [],
      newTime: "00:00",
      newSeconds: 0,
      loggedInCheck: true,
      loggedIn: false,
    };
    this.myRefs = [];
  }

  componentDidMount() {
    axios.get('/v1/schedules', {withCredentials: true})
      .then(response => {
        let missions = response.data.filter((val, i, arr) => {
          return (val.seconds >= 0);
        });
        console.log(response.data);
        missions.forEach((mission, index) => {
          mission.id = index;
          mission.time = this.secondsToHoursMinutes(mission.seconds);
        });
        this.setState({plannedMissions: missions, loggedIn: true});
      })
      .catch(error => console.log(error))
      .finally(() => this.setState({loggedInCheck: false}));
  }

  secondsToHoursMinutes = seconds => {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds - hours * 3600) / 60);
    return {hours, minutes};
  };

  editTime(id) {
    this.setState({
      isEdited: true,
      editedId: id,
    });
    const editedField = this.myRefs[id];
    this.editedField = editedField.getBoundingClientRect();
    this.editedText = editedField.innerText;
  }

  handleTimeChange = e => {
    this.setState({newSeconds: e.target.valueAsNumber / 1000, newTime: e.target.value});
  };

  closeEditMission() {
    this.setState({
      isEdited: false
    });
  }

  confirmEditMission(id, input) {
    let newSeconds = input - 60 * moment().utcOffset();
    newSeconds = newSeconds < 0 ? 24 * 3600 - newSeconds : newSeconds;
    const hangar = this.state.plannedMissions[id]["hangar_id"];
    const seconds = this.state.plannedMissions[id]["seconds"];
    axios.delete('/v1/schedules/' + hangar + '/' + seconds, {withCredentials: true})
      .then(() => axios.put('/v1/schedules', {
          "seconds": newSeconds,
          "drone_id": "cr-drone-1",
          "hangar_id": "cr-hangar-1",
          "user_id": "root@example.com"
        }, {withCredentials: true}
      ).then(() => {
        axios.get('/v1/schedules', {withCredentials: true})
          .then(response => {
            let missions = response.data.filter((val, i, arr) => {
              return (val.seconds >= 0);
            });
            missions.forEach((mission, index) => {
              mission.id = index;
              mission.time = this.secondsToHoursMinutes(mission.seconds);
            });
            this.setState({plannedMissions: missions});
            this.closeEditMission();
          })
          .catch(error => console.log(error))
      }))
  }

  addMission() {
    let seconds = this.state.newSeconds - 60 * moment().utcOffset();
    seconds = seconds < 0 ? 24 * 3600 + seconds : seconds;
    axios.put('/v1/schedules', {
        "seconds": seconds,
        "drone_id": "cr-drone-1",
        "hangar_id": "cr-hangar-1",
        "user_id": "root@example.com"
      }, {withCredentials: true}
    ).then(() => {
      axios.get('/v1/schedules', {withCredentials: true})
        .then(response => {
          let missions = response.data.filter((val, i, arr) => {
            return (val.seconds >= 0);
          });
          missions.forEach((mission, index) => {
            mission.id = index;
            mission.time = this.secondsToHoursMinutes(mission.seconds);
          });
          this.setState({plannedMissions: missions});
          this.closeEditMission();
        })
        .catch(error => console.log(error))
    })
  }

  deleteMission(id) {
    const hangar = this.state.plannedMissions[id]["hangar_id"];
    const seconds = this.state.plannedMissions[id]["seconds"];
    axios.delete('/v1/schedules/' + hangar + '/' + seconds, {withCredentials: true})
      .then(() => {
        axios.get('/v1/schedules', {withCredentials: true})
          .then(response => {
            let missions = response.data.filter((val, i, arr) => {
              return (val.seconds >= 0);
            });
            missions.forEach((mission, index) => {
              mission.id = index;
              mission.time = this.secondsToHoursMinutes(mission.seconds);
            });
            this.setState({plannedMissions: missions});
            this.closeEditMission();
          })
          .catch(error => console.log(error))
      })
  }

  renderMissionDataTable() {
    const offset = moment().utcOffset();
    return this.state.plannedMissions.map((Mission, index) => {
      const {id, time} = Mission;
      let hrs = time.hours + offset / 60;
      hrs = hrs > 23 ? hrs - 24 : hrs;
      hrs = hrs < 10 ? "0" + hrs : hrs;
      const mins = time.minutes < 10 ? "0" + time.minutes : time.minutes;
      return (
        <tr key={id}>
          <td>{id + 1}</td>
          <td ref={(ref) => {
            this.myRefs[id] = ref;
            return true;
          }}>{hrs + ':' + mins}</td>
          <td>
            <button className='Button' onClick={() => this.editTime(id)}>Edytuj</button>
          </td>
          <td>
            <button className='Button' onClick={() => this.deleteMission(id)}>Usuń</button>
          </td>
        </tr>
      )
    })
  }

  render() {
    let missionPlan;
    if (this.state.loggedInCheck) {
      missionPlan = <div></div>
    } else if (!this.state.loggedIn) {
      missionPlan = <div><Redirect to="/login"/></div>
    } else {
      missionPlan = <div className="MissionPlan">
        {this.state.isEdited ?
          <EditMission
            id={this.state.editedId}
            defTime={this.state.plannedMissions[this.state.editedId].seconds + 60 * moment().utcOffset()}
            editedField={this.editedField}
            editedText={this.editedText}
            closeEditMission={this.closeEditMission.bind(this)}
            confirmEditMission={this.confirmEditMission.bind(this)}
          />
          : null
        }
        <h3 className='PlannedMissionsTitle'>Bieżący harmonogram misji</h3>
        <table className='PlannedMissions'>
          <tbody>
          <tr>
            <th><span>Lp.</span></th>
            <th><span>Godzina misji</span></th>
            <th colSpan={"2"}><span>Akcja</span></th>
          </tr>
          {this.renderMissionDataTable()}
          <tr>
            <td colSpan={"2"}><input type='time' value={this.state.newTime} onChange={this.handleTimeChange}/></td>
            <td>
              <button className='Button' onClick={() => this.addMission()}>Dodaj</button>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    }
    return (
      <React.Fragment>
        {missionPlan}
      </React.Fragment>
    );
  }
}

export default MissionPlan;
