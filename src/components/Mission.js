import React, {Component} from 'react';
import LeafletMap from "./LeafletMap";
import Video from "./Video";
import axios from "axios";
import QuaternionWidget from "./QuaternionWidget";
import EkfWidget from "./EkfWidget";
import * as THREE from 'three';

class Mission extends Component {
  constructor(props) {
    super(props);
    this.state = {
      path: [],
    }
  }

  getPath = () => {
    axios.post('/v1/user/path', {}, {withCredentials: true})
      .then(response => {
        let pathObject = response.data.route_points;
        let path = [];
        pathObject.map(point => {
          path.push([point[1], point[2]])
        });
        this.setState({path: path},);
      })
      .catch(error => console.log(error))
  };

  componentDidMount() {
    this.getPath();
  }

  render() {
    const q = this.props.quat;
    let yaw = 0;
    let pitch = 0;
    let roll = 0;
    if(this.props.quat!=null) {
      let quat = new THREE.Quaternion(q[1],q[2],q[3],q[4]);
      let euler = new THREE.Euler();
      euler.setFromQuaternion(quat);
      roll=euler.x*180/Math.PI;
      pitch=euler.y*180/Math.PI;
      yaw=euler.z*180/Math.PI;
    }
    console.log("y: "+yaw+" p: "+pitch+" r: "+ roll);
    return (
      <div className='Mission'>
        <Video droneWaiting={this.props.droneWaiting}
               forced={this.props.forced}/>
        <div className='Widgets'>
          <EkfWidget ekf={this.props.ekf}/>
          <QuaternionWidget roll={roll} pitch={pitch} yaw={yaw}/>
        </div>
        <div className='MapContainer'>
          <LeafletMap position={this.props.position}
                      path={this.state.path}
                      currentPath={this.props.currentPath}
                      rth={this.props.rth}/>
        </div>
      </div>
    );
  }
}

export default Mission;
