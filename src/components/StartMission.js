import React, {Component} from 'react';
import Weather from "./Weather";
import dronhub_logo from '../assets/dronhub_logo.png';

class StartMission extends Component {

  render() {
    return (
      <div className='StartMission'>
        <div className='StartMissionInner' ref={popup => this.popup = popup}>
          <div className='WeatherPopup'>
            <Weather weather={this.props.weather}/>
          </div>
          <img src={dronhub_logo} className="Login-logo-dronhub" alt="dronhub_logo"/>
          <br/><br/><br/><br/>
          <p>Start misji</p>
          <p>Przed akceptacją sprawdź warunki pogodowe !</p><br/><br/>
          <button className='Button' onClick={this.props.startMission}>Rozpocznij misję</button>
          <br/>
          <button className='Button' onClick={this.props.closeStartMission}>Odwołaj misję</button>
        </div>
      </div>
    );
  }
}

export default StartMission;
