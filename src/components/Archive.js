import React, { Component } from 'react';
import Calendar from 'react-calendar';
import moment from "moment";
import CalendarIcon from '../assets/calendar.png';
import axios from "axios";

import {Redirect} from "react-router-dom";

class Archive extends Component {
    constructor(props) {
        super(props);
        this.state = {
            archivedMissions: [],
            date: new Date(),
            loggedInCheck: true,
            loggedIn: false,
        };
    }

    componentDidMount() {
      this.getMissions();
    }

  renderArchivedMissionsTable = () => {
        return this.state.archivedMissions.map((Mission) => {
            const {id, type, missionDate, status, videoUrl} = Mission;
            let trStyle;
            if (status === 'Anulowana') {
               trStyle = {color: "gray"};
            } else {
                trStyle = null;
            }
            return (
                <tr key={id} style={trStyle}>
                    <td>{type}</td>
                    <td>{moment(missionDate).format('HH:mm')}</td>
                    <td>{status}</td>
                    <td>
                        {
                            status === 'Zrealizowana' ?
                                <a href={'/videos/'+videoUrl+'.mp4'} className='Button' download={moment(missionDate).format('YYYY-MM-DD-hh-mm-ss')}>Pobierz nagranie</a>
                            : null
                        }
                    </td>
                </tr>
                )

        })
    };

    getMissions = () => {
        axios.post('/v1/flight/for', {
            date: moment(this.state.date).format('YYYY-MM-DD')
        }, {withCredentials: true})
          .then(response => {
            let missions = [];
            response.data.map((mission, i) => {
              let state = '';
              switch (mission.status) {
                case 0:
                  state = "Oczekiwanie";
                  break;
                case 1:
                  state = "Zainicjalizowana";
                  break;
                case 2:
                  state = "Zatwierdzona";
                  break;
                case 3:
                  state = "Zrealizowana";
                  break;
                case 4:
                  state = "Anulowano";
                  break;
                default:
                  state = "Brak";
              }
              missions.push({
                id: i+1,
                type: mission.type_of_mission?"Na żądanie" : "Planowana",
                missionDate: new Date(mission.datetime*1000),
                status: state,
                videoUrl: mission.flight_id,
              })
            });
            this.setState({archivedMissions: missions, loggedIn: true,});
          })
          .catch(error => console.log(error))
          .finally(()=>{
              this.setState({loggedInCheck: false,});
          })
    };

    onChange = date => this.setState({ date },this.getMissions);

    render() {
        { if (this.state.date !== null) {
            this.currentDate = moment(this.state.date).format('DD.MM.YYYY')
        } else {
            this.currentDate = moment(new Date()).format('DD.MM.YYYY')
        }}
      let archive;
      if(this.state.loggedInCheck){
        archive = <div> </div>
      } else if (!this.state.loggedIn) {
        archive = <div><Redirect to="/login"/></div>
      } else {
        archive = <div className='Archive'>
          <div className='Calendar'>
            <h3>Wybierz datę</h3>
            <img alt='' src={CalendarIcon}/>
            <div className='CurrentDate'>
              {this.currentDate}
            </div>
            <Calendar
              className={'ReactCalendar'}
              onChange={this.onChange}
              value={this.state.date}
            />
          </div>
          <div className='ArchiveTable'>
            <h3 className='ArchivedMissionsTitle'>Archiwum misji</h3>
            <table className='ArchivedMissions'>
              <tbody>
              <tr>
                <th><span>Plan misji</span></th>
                <th><span>Godzina misji</span></th>
                <th><span>Status</span></th>
                <th><span>Akcja</span></th>
              </tr>
              {this.renderArchivedMissionsTable()}
              </tbody>
            </table>
          </div>
        </div>
      }
      return (
        archive
      );
    }
}

export default Archive;