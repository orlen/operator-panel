import React, {Component} from 'react';
import axios from 'axios';

import Moment from 'react-moment';
import {Redirect} from "react-router-dom";

class ScheduleCurrent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMissions: [],
      loggedInCheck: true,
      loggedIn: false,
    }
  }

  componentDidMount() {
    const today = new Date().toISOString().split('T')[0];
    axios.post('/v1/flight/for', {
      date: today
    }, {withCredentials: true})
      .then(response => {
        this.setState({currentMissions: response.data, loggedIn: true,});
      })
      .catch(error => console.log(error))
      .finally(()=>{
        this.setState({loggedInCheck: false,});
      })
  }

  renderMissionDataTable() {
    return this.state.currentMissions.map((Mission, index) => {
      const {flight_id, type_of_mission, datetime, status} = Mission;
      const type = type_of_mission ? "Na żądanie" : "Planowana";
      const date = <Moment local format="HH:mm">{new Date(parseInt(datetime) * 1000).toISOString()}</Moment>;
      let state = '';
      switch (status) {
        case 0:
          state = "Oczekiwanie";
          break;
        case 1:
          state = "Zainicjalizowana";
          break;
        case 2:
          state = "Zatwierdzona";
          break;
        case 3:
          state = "Zrealizowana";
          break;
        case 4:
          state = "Anulowano";
          break;
        default:
          state = "Brak";
      }
      return (
        <tr key={flight_id}>
          <td>{type}</td>
          <td>{date}</td>
          <td>{state}</td>
        </tr>
      )
    })
  }

  render() {
    let scheduleCurrent;
    if(this.state.loggedInCheck){
      scheduleCurrent = <div> </div>
    } else if (!this.state.loggedIn) {
      scheduleCurrent = <div><Redirect to="/login"/></div>
    } else {
      scheduleCurrent = <div className="ScheduleCurrent">
        <h3 className='CurrentMissionsTitle'>Bieżący harmonogram misji</h3>
        <table className='CurrentMissions'>
          <tbody>
          <tr>
            <th><span>Rodzaj misji</span></th>
            <th><span>Godzina misji</span></th>
            <th><span>Status</span></th>
          </tr>
          {this.renderMissionDataTable()}
          </tbody>
        </table>
      </div>
    }
    return (
      <React.Fragment>
        {scheduleCurrent}
      </React.Fragment>
    );
  }
}

export default ScheduleCurrent;
