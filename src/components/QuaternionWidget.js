import React, {Component} from 'react';
import RollTriangle from'../assets/triangle_roll.ico'

class QuaternionWidget extends Component {
  constructor(props){
      super(props);
      this.radiusRulerRef = React.createRef();
  }

    render() {
        const invertedRotation = "roll" in this.props ? (this.props.roll) : "";
        const rotation = invertedRotation * (-1);

        const halfMoveY = "pitch" in this.props ? (this.props.pitch) : "";
        const moveY = halfMoveY * 2;

        const invertedMoveX = "yaw" in this.props ? (this.props.yaw) : "";
        const moveX = invertedMoveX * (-1);

        const roll = "rotate(" + rotation.toString() + "deg)";

        const pitch = moveY.toString() + "%";

        const yaw = moveX.toString() + "%";
    return (
        <div className='Quaternion Widget'>
            <div className='QuaternionChart'>
                <div className={'RollTriangle'}>
                    <img className='RollTriangleIcon' src={RollTriangle}/>
                    </div>
                <div className='DroneImage'>
                    <div className={'DroneLineLeft'}></div>
                    <div className={'DroneIcon'}></div>
                    <div className={'DroneLineRight'}></div>
                    </div>
                <div className='BackgroundQuaternion'>
                    <div className={'RadiusRuler'} style={{transform: "translate(-50%, -50%)" + roll}} ref={this.radiusRulerRef}>
                    <div className={'HrR-60Container'}><label>-60</label><div className={'HrR'}/></div>
                    <div className={'HrR-45Container'}><label>-45</label><div className={'HrR'}/></div>
                    <div className={'HrR-30Container'}><label>-30</label><div className={'HrR'}/></div>
                    <div className={'HrR-20Container'}><label>-20</label><div className={'HrR'}/></div>
                    <div className={'HrR-10Container'}><label>-10</label><div className={'HrR'}/></div>
                    <div className={'HrR0Container'}><label>N</label><div className={'HrR'}/></div>
                    <div className={'HrR10Container'}><label>10</label><div className={'HrR'}/></div>
                    <div className={'HrR20Container'}><label>20</label><div className={'HrR'}/></div>
                    <div className={'HrR30Container'}><label>30</label><div className={'HrR'}/></div>
                    <div className={'HrR45Container'}><label>45</label><div className={'HrR'}/></div>
                    <div className={'HrR60Container'}><label>60</label><div className={'HrR'}/></div>
                    </div>
                    <div className='HorizontalRuler' style={{marginLeft: yaw}}>
                    <hr className={'HrH165'}/>
                    <hr className={'HrH150'}/>
                    <hr className={'HrH135'}/>
                    <hr className={'HrH120'}/>
                    <hr className={'HrH105'}/>
                    <hr className={'HrH90'}/>
                    <hr className={'HrH75'}/>
                    <hr className={'HrH60'}/>
                    <hr className={'HrH45'}/>
                    <hr className={'HrH30'}/>
                    <hr className={'HrH15'}/>
                    <hr className={'HrH0'}/>
                    <hr className={'HrH15'}/>
                    <hr className={'HrH30'}/>
                    <hr className={'HrH45'}/>
                    <hr className={'HrH60'}/>
                    <hr className={'HrH75'}/>
                    <hr className={'HrH90'}/>
                    <hr className={'HrH105'}/>
                    <hr className={'HrH120'}/>
                    <hr className={'HrH135'}/>
                    <hr className={'HrH150'}/>
                    <hr className={'HrH165'}/>
                    </div>
                    <div className='VerticalRulerLeft'>
                    <hr className={'HrV10'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV5'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV0'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV-5'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV-10'}/>
                    </div>
                    <div className='VerticalRulerRight'>
                    <hr className={'HrV10'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV5'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV0'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV-5'}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr style={{width:"30%"}}/>
                    <hr className={'HrV-10'}/>
                    </div>
                    <div className={'BackgroundRuler'} style={{marginTop: pitch, transform: roll}}>
                    <div className='VerticalAddOnRuler'>
                    <hr className='HrLong10'/>
                    <hr style={{width:"30%"}}/>
                    <hr className='HrLong5'/>
                    <hr style={{width:"30%"}}/>
                    <hr className='HrLong0'/>
                    <hr style={{width:"30%"}}/>
                    <hr className='HrLong5'/>
                    <hr style={{width:"30%"}}/>
                    <hr className='HrLong10'/>
                    </div>
                    <div className='Landscape'>
                    <div className='Sky'></div>
                    <div className='Land'></div>
                    </div>
                    </div>
                    </div>
            </div>
        </div>
    );
  }
}

export default QuaternionWidget;
