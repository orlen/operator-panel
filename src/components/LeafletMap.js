import React, {Component} from 'react';
import {Map, Polyline, TileLayer, ZoomControl} from 'react-leaflet';
import L from 'leaflet';
import droneArrow from '../assets/drone_arrow.png'
import 'leaflet/dist/leaflet.css';
import RotatedMarker from "./RotatedMarker";
import axios from "axios";


class LeafletMap extends Component {
  constructor(props) {
    super(props);
    this.droneIcon = new L.Icon({
      iconUrl: droneArrow,
      iconSize: [20, 30],
      iconAnchor: [10, 15],
    });
    this.state = {zoom: 15};

  }

  setZoom = e => {
    this.setState({zoom: e.target._zoom})
  };

  abortMission = () => {
    axios.post('/v1/user/commands', {order: "abort"}, {withCredentials: true})
      .then(() => {

      })
      .catch((error) => console.log(error))
  };

  render() {
    const lat = this.props.position.gps_lat != null ? this.props.position.gps_lat : 52.5856898;
    const lon = this.props.position.gps_lon != null ? this.props.position.gps_lon : 19.6512023;
    const yaw = this.props.position.yaw != null ? this.props.position.yaw * 180 / Math.PI : 0;
    return (
      <div className='LeafletMap'>
        <Map
          zoomControl={false}
          onZoomEnd={this.setZoom}
          style={{height: "60vh", width: "100%"}}
          zoom={this.state.zoom}
          center={[lat, lon]}>
          <RotatedMarker
            position={[lat, lon]}
            icon={this.droneIcon}
            rotationAngle={yaw}
            rotationOrigin={'center'}
          />
          <ZoomControl position="bottomleft"/>
          <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
          <Polyline positions={this.props.path} color={'blue'} opacity={0.3}/>
          <Polyline positions={this.props.currentPath} color={'red'}/>
        </Map>
        <br/>
        <div className='MapButtons'>
          {!this.props.rth ? <button className='Button' onClick={this.abortMission}>Wróć do dronhuba</button> :
            <button className='Button' disabled={true}>Dron wraca do hangaru</button>}
        </div>
      </div>
    );
  }
}

export default LeafletMap;
