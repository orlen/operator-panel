import React, {Component} from 'react';
import {Link} from "react-router-dom";

import axios from "axios";


class Logout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedOut: false,
    }
  }

  componentDidMount() {
    axios.post('/v1/user/logout', {}, {withCredentials: true})
      .then(() => {
        this.setState({loggedOut: true});
      })
      .catch((error) => {
        if (error.response.status === 401){
          this.setState({loggedOut: true});
        }
        console.log(error);
      })

  }

  render() {
    let state = <h4>Wylogowywanie...</h4>
    if (this.state.loggedOut) {
      state = <h4>Wylogowano pomyślnie</h4>
    }
    return (
      <div className='LogoutScreen'>
        <div>
          {state}
        </div>
        <div>
          <Link to={'/login'} className="NavigationLink">Zaloguj ponownie</Link>
        </div>
      </div>
    );
  }
}

export default Logout;
