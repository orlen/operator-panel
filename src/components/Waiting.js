import React, {Component} from 'react';
import axios from 'axios';

import Timer from "react-compound-timer"
import {Redirect} from "react-router-dom";

class Waiting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flights: {},
      nextFlight: 0,
      loggedInCheck: true,
      loggedIn: false,
    };
  }

  demandFlight = () => {
    axios.put('/v1/flight/new', {
      "drone_id": "cr-drone-1",
      "hangar_id": "cr-hangar-1"
    }, {withCredentials: true})
      .then(response => {
      })
      .catch(error => console.log(error));
  };

  checkTomorrowFlights = () => {
    let nextDate = new Date();
    nextDate.setDate(nextDate.getDate() + 1);
    const tomorrow = nextDate.toISOString().split('T')[0];
    const now = Math.trunc(new Date().getTime() / 1000);
    axios.post('/v1/flight/for', {
      date: tomorrow
    }, {withCredentials: true})
      .then(response => {
        this.setState({
          flights: [...this.state.flights, ...response.data],
        }, () => {
          for (let i = 0; i < this.state.flights.length; ++i) {
            if (this.state.flights[i].datetime > now) {
              this.setState({nextFlight: this.state.flights[i].datetime});
              return;
            }
          }
        })
      })

  };
  checkTodayFlights = () => {
    const today = new Date().toISOString().split('T')[0];
    const now = Math.trunc(new Date().getTime() / 1000);
    axios.post('/v1/flight/for', {
      date: today
    }, {withCredentials: true})
      .then(response => {
        this.setState({
          flights: response.data,
          loggedIn: true,
        }, () => {
          for (let i = 0; i < this.state.flights.length; ++i) {
            if (this.state.flights[i].datetime > now) {
              this.setState({nextFlight: this.state.flights[i].datetime});
              return;
            }
          }
          if (this.state.nextFlight === 0) {
            this.checkTomorrowFlights();
          }
        })
      })
      .catch(error => {
        if (error.response.status === 401) {
          this.setState({loggedIn: false})
        }
      })
      .finally(() => {
        this.setState({loggedInCheck: false})
      })
  };

  componentDidMount() {
    this.checkTodayFlights();
  }

  render() {
    const now = Math.trunc(new Date().getTime() / 1000);
    let waiting;
    if (this.state.loggedInCheck) {
      waiting = <div> </div>
    } else if (!this.state.loggedIn) {
      waiting = <div><Redirect to="/login"/></div>
    } else {

      waiting = <div className='Waiting'>
        <p>Oczekiwanie na misję</p>
        {this.state.nextFlight ? <Timer
          initialTime={(this.state.nextFlight - now) * 1000}
          direction="backward"
        >
          {() => (
            <React.Fragment>
              <Timer.Hours/> : <Timer.Minutes/> : <Timer.Seconds/>
            </React.Fragment>
          )}
        </Timer> : <p>Brak misji w najbliższym czasie</p>}
        <input className="Button" onClick={this.demandFlight} type="button"
               value="Wykonaj misję na żądanie"/>
      </div>
    }
    return (
      <React.Fragment>
        {waiting}
      </React.Fragment>
    );
  }
}

export default Waiting;