import React, { Component } from 'react';
import {Link} from "react-router-dom";
import dronhub_logo from '../assets/dronhub_logo.png';
import {withRouter} from 'react-router-dom';
class Navbar extends Component {
    constructor() {
        super();

        this.state = {
          showScheduleMenu: false,
          underlineOffset: 0,
        };

        this.showScheduleMenu = this.showScheduleMenu.bind(this);
        this.closeScheduleMenu = this.closeScheduleMenu.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.setUnderline = this.setUnderline.bind(this);
        this.scheduleMenuRef = React.createRef();
        this.scheduleMenuDropdownRef = React.createRef();
        this.manageLinkRef = React.createRef();
        this.scheduleLinkRef = React.createRef();
        this.archiveLinkRef = React.createRef();
        this.selectedMenuItem= React.createRef();
        this.navBarRef = React.createRef();
    }

    showScheduleMenu(event) {
        event.preventDefault();
        this.setState({ showScheduleMenu: true });
        if (this.scheduleMenuRef.current) {
            this.xScheduleMenu = this.scheduleMenuRef.current.offsetLeft;
        }
    }

    closeScheduleMenu() {
        this.scheduleMenuDropdownRef.current.display = 'none';
        this.setState({ showScheduleMenu: false });
    }

  handleClickOutside(event) {
    if (this.scheduleMenuDropdownRef.current && !this.scheduleMenuDropdownRef.current.contains(event.target)) {
      this.closeScheduleMenu();
    }
  }

  setUnderline() {
      let offset = this.manageLinkRef.current.offsetLeft;
    const height = this.navBarRef.current.offsetHeight;
    let width = this.manageLinkRef.current.offsetWidth;

    if (window.location.pathname.includes('manage')) {
      width = this.manageLinkRef.current.offsetWidth;
      offset = this.manageLinkRef.current.offsetLeft;
    } else if (window.location.pathname.includes('schedule')) {
      width = this.scheduleLinkRef.current.offsetWidth;
      offset = this.scheduleLinkRef.current.offsetLeft;
    } else if (window.location.pathname.includes('archive')) {
      width = this.archiveLinkRef.current.offsetWidth;
      offset = this.archiveLinkRef.current.offsetLeft;
    }
    this.setState({underlineOffset: offset});
    this.selectedMenuItem.current.style.width =  width + 'px';
    this.selectedMenuItem.current.style.height = height + 'px';
  }

  componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        window.addEventListener("resize", this.setUnderline);
        this.setUnderline();
    }
  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.setUnderline();
    }
  }

    render() {
        return (
                <nav className="Navbar" ref={this.navBarRef}>
                    {this.state.showScheduleMenu
                        ? (<div className='ScheduleMenuOuter'> </div>)
                        : null
                    }
                    <div className='SelectedMenuItem' style={{marginLeft: this.state.underlineOffset + 'px'}} ref={this.selectedMenuItem}> </div>
                        <div className='MainMenu'>
                            <div className='ManageLink'><Link to={'/manage'} className="NavigationLink" ref={this.manageLinkRef}>Zarządzanie Misją</Link></div>
                            <div ref={this.scheduleLinkRef}><Link to={'/schedule'} onClick={this.showScheduleMenu} className="NavigationLink" ref={this.scheduleMenuRef}>Harmonogram misji</Link></div>
                            {
                                this.state.showScheduleMenu
                                ? (
                                <div className="ScheduleMenu" style={{marginLeft: this.xScheduleMenu + 'px'}} ref={this.scheduleMenuDropdownRef}>
                                    <a href="#" onClick={this.closeScheduleMenu} className="NavigationLink" ref={this.scheduleMenuRef}>Harmonogram misji</a>
                                    <Link to={'/schedule_current'}  onClick={this.closeScheduleMenu}  className="NavigationLink">Bieżący</Link>
                                    <Link to={'/schedule_plan'}  onClick={this.closeScheduleMenu} className="NavigationLink">Plan</Link>
                                </div>
                                ) : null
                                }
                            <div ref={this.archiveLinkRef}><Link to={'/archive'} className="NavigationLink">Archiwum</Link></div>
                        </div>
                    <div className='MainLogo'>
                        <img src={dronhub_logo} className="Login-logo-dronhub" alt="dronhub_logo" />
                    </div>
                    <div className='Logout'>
                        <Link to={'/logout'} className="NavigationLink">Wyloguj</Link>
                    </div>
                </nav>
        );
    }
}

export default withRouter(Navbar);
