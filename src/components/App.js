import React, {Component} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import Login from './Login';
import '../index.css';
import Manage from './Manage';
import ScheduleCurrent from './ScheduleCurrent';
import MissionPlan from './MissionPlan';
import Archive from './Archive';
import Navbar from './Navbar';
import Logout from "./Logout";

const LoginPage = () => (
  <Login/>
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state={showNavbar: true};
    this.pathnamePolling = null;
  }

  componentDidMount() {
    this.pathnamePolling = setInterval(()=>{
      if (location.pathname === '/login') {
        this.setState({showNavbar: false});
      } else if (location.pathname === '/logout') {
        this.setState({showNavbar: false});
      } else {
        this.setState({showNavbar: true});
      }
    }, 500);
  }
  componentWillUnmount() {
    clearInterval(this.pathnamePolling);
  }

  render() {
    return (
      <Router>
        <div className="App">
          {this.state.showNavbar?<Navbar/>:null}
          <Route path="/login" component={LoginPage}/>
          <Switch>
            <Route path='/manage' component={Manage}/>
            <Route path='/logout' component={Logout}/>
            <Route path='/schedule_current' component={ScheduleCurrent}/>
            <Route path='/schedule_plan' component={MissionPlan}/>
            <Route path='/archive' component={Archive}/>
            <Route exact path='/' render={() => <div><Redirect to={'/manage'} push/></div>}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;