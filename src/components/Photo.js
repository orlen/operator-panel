import React, { Component } from 'react';
import dronhub_logo from '../assets/dronhub_logo.png';

class Photo extends Component {
    constructor(props) {
        super(props);
    }

    savePhoto = () => {
        const saveLink = document.createElement('a');
        saveLink.download = this.fileName;
        saveLink.href = this.props.photo;
        document.body.appendChild(saveLink);
        saveLink.click();
        document.body.removeChild(saveLink);
        this.props.closePhotoDron();
    };




    render() {
        const today = new Date();
        this.fileName = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate() + '-' + today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds() + '.png';

        return (
            <div className='Photo'>
                <div className='PhotoInner'>
                    <img src={dronhub_logo} className="Login-logo-dronhub" alt="dronhub_logo"/>
                    <p>{this.fileName}</p>
                    <img src={this.props.photo}/>
                    <button className='Button' onClick={this.savePhoto}>Zapisz</button>
                    <button className='Button' onClick={this.props.closePhotoDron}>Zamknij</button>
                </div>
            </div>
        );
    }
}

export default Photo;
