import React, {Component} from 'react';
import dronhub_logo from '../assets/dronhub_logo.png';
import cr_logo from '../assets/cr_logo.png';
import '../index.css';
import axios from 'axios';
import {Redirect} from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
        // debug
      loggedIn: true,
    }
  }


  handleLoginSubmit = (e) => {
    e.preventDefault();
    const {login, password} = this.state;
    axios.post('/v1/user/login', {
      login,
      password,
    }, {
      withCredentials: true
    }).then(response => {
      this.setState({loggedIn: true});
      window.location.reload();
    }).catch(error => console.log(error))
  };

  handleLoginChange = e => {
    this.setState({login: e.target.value})
  };

  handlePasswordChange = e => {
    this.setState({password: e.target.value})
  };

  render() {
    let login;
    if (this.state.loggedIn) {
      login = <Redirect to={'/manage'}/>
    } else {
      login = <div className="Login">
        <div className="Login-header">
          <img src={dronhub_logo} className="Login-logo-dronhub" alt="dronhub_logo"/>
          <p>Panel operatora</p>
        </div>
        <form onSubmit={this.handleLoginSubmit}>
          <input className="Login-username" value={this.state.login}
                 onChange={this.handleLoginChange} placeholder="Login"/>
          <input className="Login-password" type="password" value={this.state.password}
                 onChange={this.handlePasswordChange} placeholder="Hasło"/>
          <input className="Login-submit Button" type="submit" value="Zaloguj"/>
        </form>
        <div className="Login-footer">
          <p>Powered by &nbsp;
            <img src={cr_logo} className="Login-logo-cr" alt="cr_logo"/>
          </p>
        </div>
      </div>
    }
    return (
      <React.Fragment>
        {login}
      </React.Fragment>
    );
  }
}

export default Login;