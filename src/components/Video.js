import React, {Component} from 'react';
import Photo from "./Photo";
import axios from 'axios';

class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPhotoMade: false,
      photo: null
    };
    this.videoRef = React.createRef();
  }

  toggleDroneWaiting = e => {
    if (!this.props.droneWaiting) {
      axios.post('/v1/user/commands', {order: "stop"}, {withCredentials: true})
        .catch((error) => console.log(error))
    } else {
      axios.post('/v1/user/commands', {order: "resume"}, {withCredentials: true})
        .catch((error) => console.log(error))
    }
  };

  toggleIsPhotoMade = () => {
    this.setState({
      isPhotoMade: !this.state.isPhotoMade
    });
  };

  makePhoto = (e) => {
    const canvas = document.createElement('canvas');
    const video = this.videoRef.current;

    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    const ctx = canvas.getContext("2d").drawImage(video, 0, 0, video.videoWidth, video.videoHeight);

    const photo = canvas.toDataURL("image/png");

    this.setState({
      photo: photo
    });

    this.toggleIsPhotoMade()
  };

  render() {
    return (
      <div className='VideoContainer'>
        {this.state.isPhotoMade ?
          <Photo closePhotoDron={this.toggleIsPhotoMade.bind(this)} photo={this.state.photo}/>
          : null
        }

        <div className='Video'>
          <video ref={this.videoRef} width="100%" height="100%" crossOrigin='anonymous' autoPlay muted>
            <source src='/stream.ogg'/>
            <em>Sorry, your browser doesn't support HTML5 video.</em>
          </video>
        </div>
        <br/>
        <div className='VideoButtons'>
          {this.props.forced ? <button className='Button' disabled={true}>Lot wymuszony</button> :
            this.props.droneWaiting ?
              <button className='Button' onClick={this.toggleDroneWaiting}>Wznów lot</button>
              :
              <button className='Button' onClick={this.toggleDroneWaiting}>Zatrzymaj się</button>
          }
          <button className='Button' onClick={this.makePhoto}>Wykonaj zdjęcie</button>
        </div>
      </div>
    );
  }
}

export default Video;
