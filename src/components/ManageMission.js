import React, {Component} from 'react';
import StartMission from "./StartMission";
import Mission from "./Mission";
import Waiting from "./Waiting";

import axios from "axios";


class ManageMission extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // when debug mode waiting is set to false and start mission is set to true
            waiting: false,
            showStartMission: false,
            startMission: true,
        };
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.message !== prevProps.message) {
            if (this.props.message === 'confirm') {
                this.toggleStartMission(true);
            }
        }
        if (this.props.isFlight !== prevProps.isFlight) {
            if (this.props.isFlight) {
                this.setState({startMission: true, waiting: false, showStartMission: false, })
            } else {
                this.setState({startMission: false, waiting: true, showStartMission: false, })
            }
        }
    }

    toggleWaiting(val) {
        this.setState({
            waiting: val
        });
    }

    cancelMission() {
        axios.post('/v1/user/commands', {order: "cancel"}, {withCredentials: true})
          .then(() => {
          })
          .catch((error) => console.log(error))
          .finally(() => {
              this.setState({
                  showStartMission: !this.state.showStartMission
              });
          })
    }

    toggleStartMission() {
        this.setState({
            showStartMission: !this.state.showStartMission
        });
    };

    startMission() {
        axios.post('/v1/user/commands', {order: "authorize"}, {withCredentials: true})
          .then(() => {
              this.setState({
                  startMission: true
              });
          })
          .catch((error) => console.log(error))
          .finally(() => {
              this.setState({
                  showStartMission: !this.state.showStartMission
              });
          });

        this.toggleWaiting(!this.state.waiting);
        this.toggleStartMission(!this.state.showStartMission);
    }

    render() {
        return (
          <div className='MainContainer'>
              {this.state.waiting ?
                <Waiting
                  toggleStartMission={this.toggleStartMission.bind(this)}
                />
                : null
              }
              {this.state.showStartMission ?
                <StartMission weather={this.props.weather}
                              startMission={this.startMission.bind(this)}
                              closeStartMission={this.cancelMission.bind(this)}
                />
                : null
              }
              {this.state.startMission ?
                <Mission position={this.props.position}
                         currentPath={this.props.currentPath}
                         quat={this.props.position.quat}
                         ekf={this.props.stream.ekf}
                         droneWaiting={this.props.droneWaiting}
                         rth={this.props.rth}
                         forced={this.props.forced}/>
                : null
              }
          </div>
        );
    }
}

export default ManageMission;