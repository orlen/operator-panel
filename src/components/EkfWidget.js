import React from "react";
import Chart from "react-google-charts";



class EkfWidget extends React.Component {
  constructor(props) {
    super(props);
    this.data = [
      ["Element", "Value", {role: "style"}],
      ["Velocity", this.props.ekf.velocity_variance!=null?this.props.ekf.velocity_variance:0, "green"],
      ["Pos(H)", this.props.ekf.pos_horiz_variance!=null?this.props.ekf.pos_horiz_variance:0, "green"],
      ["Pos(V)", this.props.ekf.pos_vert_variance!=null?this.props.ekf.pos_vert_variance:0, "green"],
      ["Compass", this.props.ekf.compass_variance!=null?this.props.ekf.compass_variance:0, "green"],
    ];
  }

  render() {
    let colored_data = this.data;
    this.data.map((item, i) => {
      if(item[1]>=0.8){
        colored_data[i][2]="red";
      } else if (item[1]>=0.5){
        colored_data[i][2]="yellow";
      }
    });
    return (
      <div className="Ekf Widget">
          <div className={'EkfChart'}>
            <Chart
              chartType="ColumnChart"
              width="100%"
              height="100%"
              options={{
                backgroundColor: '#444444',
                legend:{
                  position: 'none'
                },
                hAxis: {
                  textStyle: {
                    color: 'white',
                    fontSize: 8
                  }
                },
                vAxis: {
                  textStyle: {
                    color: 'white'
                  },
                  ticks:[0.2, 0.4, 0.5, 0.6, 0.8, 1],
                  minValue: 0,
                  maxValue: 1,
                }
              }}
              data={colored_data}
            />
          </div>
      </div>
    );
  }
}

export default EkfWidget;
