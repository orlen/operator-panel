import React, {Component} from 'react';

class Weather extends Component {
  render() {
      let tempClassStyle, windClassStyle, rainClassStyle, kpIndexStyle;
    const temp = "temp_pv" in this.props.weather ? (this.props.weather.temp_pv).toFixed(2) : "???";
    const hum = "wilg_pv" in this.props.weather ? this.props.weather.wilg_pv: "???";
    const press = "cisn" in this.props.weather ? this.props.weather.cisn : "???";
    const windSpeed = "predkosc_wiatr" in this.props.weather ? this.props.weather.predkosc_wiatr : 0;
    const windDeg = "kier_wiatr" in this.props.weather ? this.props.weather.kier_wiatr : '-';
    const rain = "opad" in this.props.weather ? this.props.weather.opad+'mm' : '-';
    const kpIndex = "kpIndeks" in this.props.weather ? this.props.weather.kpIndex : '-';
    let direction = '?';

    switch(Math.floor((windDeg+22.5)/45)){
      case 0: direction = 'N'; break;
      case 1: direction = 'NE'; break;
      case 2: direction = 'E'; break;
      case 3: direction = 'SE'; break;
      case 4: direction = 'S'; break;
      case 5: direction = 'SW'; break;
      case 6: direction = 'W'; break;
      case 7: direction = 'NW'; break;
      case 8: direction = 'N'; break;
      default: direction = '?';
    }

    if (temp < 0 || temp >= 35) {
        tempClassStyle = {color: 'red'};
    } else if ((temp >= 0 && temp < 9) || (temp >= 30 && temp < 35)){
        tempClassStyle = {color: 'orange'};
    } else if (!(temp >= 9 || temp <= 30)) {
        tempClassStyle = {color: 'red'};
    }

    if (windSpeed >= 6 || windSpeed < 9) {
        windClassStyle = {color: 'orange'};
    } else if (windSpeed >= 9) {
        windClassStyle = {color: 'red'};
    }

    if (rain >= 30) {
        rainClassStyle = {color: 'red'};
    }

    if (kpIndex > 3 && kpIndex <= 4) {
      kpIndexStyle = {color: 'orange'};
    } else if (kpIndex > 4) {
      kpIndexStyle = {color: 'red'};
    }


    return (
      <div className='Weather Info'>
        <p className='InfoTitle'>Pogoda</p>
        <div className='InfoContent'>
          <div className='LeftColumn'>
            <p>Temperatura:</p>
            <p>Ciśnienie :</p>
            <p>Wilgotność :</p>
            <p>Prędkość wiatru :</p>
            <p>Kierunek wiatru :</p>
            <p>Suma opadów : </p>
            <p>Indeks Kp: </p>
          </div>
          <div className='RightColumn'>
            <p><b style={tempClassStyle}>{temp}</b> °C </p>
            <p><b>{press}</b> hPa</p>
            <p><b>{hum}</b> % </p>
            <p><b style={windClassStyle}>{windSpeed}</b> m/s</p>
            <p><b>{direction}</b></p>
            <p><b style={rainClassStyle}>{rain}</b></p>
            <p><b style={kpIndexStyle}>{kpIndex}</b></p>
          </div>
        </div>
      </div>
    );
  }
}

export default Weather;
