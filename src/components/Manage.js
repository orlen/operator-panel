import React, {Component} from 'react';
import ManageMission from "./ManageMission";
import Weather from "./Weather";
import Dron from "./Dron";
import Dronhub from "./Dronhub";
import axios from "axios";
import {w3cwebsocket as W3CWebSocket} from 'websocket';

const client = new W3CWebSocket((location.protocol === 'http:' ? 'ws:' : 'wss:') + location.hostname + '/v1/user/websocket');

class Manage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weather: {},
      droneData: this.initStream(),
      heartbeat: this.initHeartbeat(),
      message: '',
      isFlight: false,
      currentPath: [],
      droneWaiting: false,
      rth: false,
      forced: false,
    }
  }

  checkWeather = () => {
    axios.post('/v1/weather', {}, {withCredentials: true})
      .then(response => {
        this.setState({weather: response.data});
      })
      .catch(error => console.log(error))
  };

  getCookieValue = (name) => {
    let found = document.cookie.split(';').filter(c => c.trim().split("=")[0] === name);
    return found.length > 0 ? found[0].split("=")[1] : null;
  };

  initStream = () => {
    return {
      "stream": {
        "system": {"satellites_visible": null, "state": "initializing", "route_id": null},
        "position": {
          "gps_lat": null,
          "gps_lon": null,
          "alt_amsl": null,
          "alt_local": null,
          "vel_ned": null,
          "yaw": null,
          "quat": null,
          "hdop": null
        },
        "waypoint": {"next_point": null, "next_point_route_id": null, "route_progress": null},
        "battery": {"battery_remaining": null, "battery_voltage": null, "battery_current": null},
        "ekf": {
          "velocity_variance": null,
          "pos_horiz_variance": null,
          "pos_vert_variance": null,
          "compass_variance": null
        }
      }
    }
  };
  initHeartbeat = () => {
    return {
      'drone_id': '-',
      'state': '-',
      'drone_battery': {
        'charging': false,
        'current': 0,
        'voltage': 0,
      }
    }
  };


  componentDidMount() {
    this.checkWeather();
    axios.post('/v1/flights/outstanding', {}, {withCredentials: true})
      .then((response => {
        if (response.data.flights_outstanding) {
          this.setState({message: 'confirm'})
        }
      }));
    client.onopen = () => {
      const cookie = this.getCookieValue("session_id");
      client.send(JSON.stringify({session: cookie}));
    };
    client.onmessage = (message) => {
      const droneData = JSON.parse(message.data);
      console.log(droneData);
      if (droneData.message_type === 'heartbeat') {
        this.setState({heartbeat: droneData.heartbeat});
      }
      if ('stream' in droneData) {
        const state = droneData.stream.system.state;
        const flightStates = ["takeoff", "cruise", "approach", "landing"];
        if (flightStates.some(el => state.includes(el))) {
          console.log('DRONE STARTED!' + droneData.stream.system.state);
          this.setState({droneData: droneData, isFlight: true});
          let lat = droneData.stream.position.gps_lat;
          let lon = droneData.stream.position.gps_lon;
          this.setState(prevState => {
            return {currentPath: [...prevState.currentPath, [lat, lon]]}
          });
          if (state.includes("waiting")) {
            this.setState({droneWaiting: true})
          } else {
            this.setState({droneWaiting: false})
          }
          if (state.includes("rth")) {
            this.setState({rth: true})
          } else {
            this.setState({rth: false})
          }
        }
      } else if ('type' in droneData) {
        if (droneData.type === "flight-confirm") {
          this.setState(
            {message: 'confirm'}
          )
        } else if (droneData.type === "flight-finished") {
          this.setState(
            {isFlight: false}
          )
        } else if (droneData.type === "forced-flight") {
          this.setState(
            {forced: true}
          )
        }
      }
    };
    client.onclose = () => {
      console.log("closed");
    }
  }

  render() {
    return (
      <div className='Manage'>
        <div className='ManageMain'>
          <ManageMission weather={this.state.weather}
                         position={this.state.droneData.stream.position}
                         stream={this.state.droneData.stream}
                         message={this.state.message}
                         isFlight={this.state.isFlight}
                         currentPath={this.state.currentPath}
                         droneWaiting={this.state.droneWaiting}
                         rth={this.state.rth}
                         forced={this.state.forced}/>
        </div>
        <div className='ContainerInfo'>
          <div className='ManageInfo'>
            <Weather weather={this.state.weather}/>
            <Dron system={this.state.droneData.stream.system}
                  battery={this.state.droneData.stream.battery}
                  flight={this.state.droneData.stream.position}/>
            <Dronhub
              heartbeat={this.state.heartbeat}/>
          </div>
        </div>
      </div>
    );
  }
}

export default Manage;